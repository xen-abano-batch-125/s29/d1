//express.js

//load express module to be able to use express properties and methods
let express = require("express");

//store port 4000 in a variable
const PORT = 4000;

//app is now our server
let app = express();

//middlewares (bodyparser) - should be placed before routes
	//setup for allowing the server to handle data from request
app.use(express.urlencoded({extended:true}));
app.use(express.json()); //appply app read JSON data


//routes
app.get("/", (req,res)=>res.send(`HELLO WORLD`));

//mini activity
	//create a route will show a message "Hello from the /hello endpoint"
	app.get("/hello",(req,res)=>res.send());

//mini activity
	app.post("/greetings", (req,res) => {
		console.log(req.body);

		res.send(`Hello there, ${req.body.firstName}!`);
	});


app.listen(PORT, ()=>{
	console.log(`Serve running at port ${PORT}`)
});